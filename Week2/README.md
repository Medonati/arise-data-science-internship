**Challenge**

**Your challenge this week is a tough one. You are being tasked with investigating a collection of clients that applied for a loan but were declined. You are to use a**
**K-means clustering to identify clusters of clients within the collection. The data set is titled ‘week3_challenge.csv’ and can found in the week 3 folder.**

● Use pandas to read in the dataset and store it as a pandas dataframe.(hint: read_csv()).

● Remove columns from the dataframe that have more than 30% of their values missing (answer for number of columns removed is required in form).

● Create a new dataframe containing only the numeric columns (integers and floats) in the dataset (hint: select_dtypes(include=[]) (answer for number of
numeric columns, remaining after removing the columns with too many missing values, is required in form).

● Replace null values in the numeric dataframe with 0.

● Scale the numeric dataframe.

● Perform a PCA reduction on the numeric dataframe using sklearn.decomposition’s PCA function. Only include the first 5 principal components and store
the results in a pandas dataframe. Print the total variance explained by the first 5 principal components (answer required in form).

● Use sklearn’s KMeans function to cluster the numeric PCA dataframe into 3 clusters and plot the clusters.

● Reconnect the final clusters of each observation to the original dataframe and perform some exploratory analysis. An example would be to calculate the
mean income of each cluster (this can be done using a pandas group by).