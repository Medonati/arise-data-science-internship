**Challenge**

**You have been tasked with creating two queries.**

**The first is to write a query that first extract only clients that received their first loan in January 2018. You then have to find the number**
**of those clients that received/took out a loan in the following months of 2018. If a client took more than one loan in a month, they**
**should only be counted once. This is a measure of client retention. Please display your results in Google Data Studio (GDS), please follow**
**this link to get to GDS.**

**Your second task is to produce a very similar report but for clients that had a bill payment in March 2018. You have to show the number**
**of those clients that made at least one bill payment in the remaining months of 2018. On top of this, you have to show whether or not**
**the bill payment clients have a loan within 2018 or not. Make sure that the second part is displayed in your results (hint: two category**
**bar plot).**

**Please display your findings on a new page within your GDS report (you will have a page for loans and a page for bill payments).**