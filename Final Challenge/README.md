**Final challenge**

**This two week challenge is going to test your full capabilities as a data scientist. You are going to be required to wrangle a dataset using**
**Bigquery, develop two loan pre-delinquency models, comprehensively compare the two models, and explain which one you would implement.**
**A pre-delinquency model is a model that is triggered after a loan is disbursed. The model uses features developed after loan disbursement to predict whether or not a loan will be defaulted on or paid back.**
**You are going to build both a random forest classifier and a neural network. Both models should be trained and tested on the same data.**
**You will then have to properly compare the models.**
**The data that will be used for training and testing the models sits in Bigquery, but you will have to perform some transformations and manipulations to wrangle the data correctly.**

**We have our data and our target variable. The next step is to build the models. You are required to build a random forest and neural network, that both perform a binary classification on each loan. The classification is where or not a loan will be defaulted on (1) or not (0).**
